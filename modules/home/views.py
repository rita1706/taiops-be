from flask_restful import Resource


class Root(Resource):
    def get(self):
        return {
            "version": "v1 - Development",
            "description": "Taiops - project"
        }
