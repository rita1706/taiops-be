import os
import ast

# Environment
ENV = "development"

# Debugging configuration
DEBUG = True

# App Secret Key
SECRET_KEY = "986f18c894c73ed51aee9ee1c2a3eca620a5a75ae80670e8"

# TWITTER API
TWITTER_CONNECTION = {
    "ACCESS_TOKEN" : "222426845-hJ2m970eYHx3BQHhti5n1nyvhp8ky7wofZZbtOfC",
    "ACCESS_TOKEN_SECRET" : "2YzNo4furVsIfkgVD1WDTigdSPYLwlXzsFKAdXorsP4Qb",
    "CONSUMER_KEY" : "AWqmImFleBshneEkERnRbejQ9",
    "CONSUMER_SECRET" : "wC2dOaqVymCdyK3XpXaII8tauEPZbU08xUctEUN0G57CZ8LT3R",
    "consumer_key" : "AWqnImFleBsineEkERnRbejQ9",
    "consumer_secret" : "wC2dOaqVymCdyK3XpXaII8tapEPZaU08xUctEUN0G57CZ8LT3R"
}

# API documentation URL
DOCS_URL = "/docs"

# Modules directory
MODULES_DIR = "modules"

# REST resource rules object in modules
RESOURCES_OBJECT = "resources"

# Bundle validation errors
BUNDLE_ERRORS = True

# Default page limit
DEFAULT_PAGE_LIMIT = 10

CASSANDRA_KEYSPACE = os.environ.get('CASSANDRA_KEYSPACE','apache_bugs_raw')
# Cassandra connection
CASSANDRA_CONNECTION = {
    "CLUSTER": os.environ.get('CASSANDRA_HOSTS','210.180.118.110').split(','),
    "KEYSPACE": CASSANDRA_KEYSPACE,
    "COMPRESSION": True,
    "PROTOCOL_VERSION": 3
}


