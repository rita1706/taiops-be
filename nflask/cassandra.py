import os
from cassandra.cqlengine import connection
from cassandra.cqlengine.management import sync_table
from cassandra.cqlengine.models import Model as BaseModel
from nflask.elasticsearch import create_index


def init_db(app):
    os.environ['CQLENG_ALLOW_SCHEMA_MANAGEMENT'] = "1"
    # Setup cassandra connection used by object mapper
    app.logger.info("Initializing Cassandra DB Connection...")
    config = app.config['CASSANDRA_CONNECTION']

    connection.setup(
        config['CLUSTER'],
        config['KEYSPACE'],
        protocol_version=config['PROTOCOL_VERSION'],
        compression=config['COMPRESSION'])


def sync_db(app, prefix):
    # Map models files in modules directories
    import os
    from nflask.loaders import import_modules
    keyspace = app.config['CASSANDRA_KEYSPACE']

    # Get all models in modules dirs
    module_list = list(
        dict(
            path="{}/{}/models".format(prefix, module),
            name="{}_models".format(module)
        ) for module in os.listdir(prefix)
        # flake8: noqa
        if not module.endswith(".py") and
        module != "__pycache__" and
        module != ".gitkeep" and
        module != "auth" and
        module != "home" and
        module != "modules" and
        module != ".DS_Store")
    # Load models
    imported_modules = list(
        import_modules(
            app,
            module["path"],
            name=module["name"]
        ) for module in module_list
    )

    for module in imported_modules:
        if module is not None:
            # Get all object in module
            models = list(
                filter(
                    lambda model: not model.startswith("__"),
                    module.__dict__.keys()
                )
            )
            # Sync all model in models.py
            for item in models:
                model = getattr(module, item)
                # Load model if it is children of cassandra Model class
                if model.__class__.__name__ == 'ModelMetaClass':
                    
                    # Load model if it is not abstract
                    if not model.__abstract__:
                        app.logger.info(
                            "Synchronizing {}: {}".format(
                                module.__name__, item))
                        # Synchronizing table in module
                        sync_table(model)

                    # Create new index
                    if model.__use_elastic__ or model.__use_elassandra__:
                        # Get elastic setting
                        #elastic_index_name = model.__elastic_index__
                        
                        #if elastic_index_name is not None:
                            # Create index if elastic settings is found
                        create_index(
                            app,                                
                            model)

