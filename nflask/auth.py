import json
import random
import string
from flask import current_app as app, request
from datetime import datetime, timedelta
from itsdangerous import JSONWebSignatureSerializer
from functools import wraps
from nflask.exceptions import Unauthorized, TokenNotFound, InvalidTokenType, DontHavePermission, ExpiredSession

def generate_token(user):
    s = JSONWebSignatureSerializer(app.config['SECRET_KEY'])
    _user = {
        "id": str(user.profile_id),
        "email": str(user.username),
        "time": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    }
    token = s.dumps(_user)

    return token.decode('ASCII')

def generate_session(user, token):
    _created = datetime.now()
    _expired = _created + timedelta(hours=4)

    # permission = get_permissions(str(user.id))
    # user_level = get_user_level(str(user.id))

    # if user.no_limit:
    #     expired_session = 0
    # else:
    expired_session = _expired.strftime("%Y-%m-%d %H:%M:%S")
    
    # Generate session data with active user
    # information so frontend should not
    # request multiple times
    return {
        "token": token,
        "created": _created.strftime("%Y-%m-%d %H:%M:%S"),
        "expired": expired_session,
        "user": {
            "id": str(user.id),
            "email": user.email,
            "nama_lengkap": user.fullname(),
            # "id_unit_kerja": str(user.id_unit_kerja),
            # "nama_unit_kerja": str(user.nama_unit_kerja),
            # "nama_unit_kerja": str(user.nama_unit_kerja),
            # "id_organisasi": str(user.id_organisasi),
            # "id_jabatan": str(user.id_jabatan),
        },
        # "user_level": user_level,
        # "permissions": permission,
        #"menu": menu
    }


def save_session(session):
    # Get session prefix from config
    session_prefix = app.config['SESSION_PREFIX']

    # Clear all previous session
    email = session["user"]["email"]
    clear_session(email=email)

    # Save session information
    app.redis.set(
        "{}:{}:{}".format(
            session_prefix,
            session["token"],
            session["user"]["email"]
        ),
        json.dumps(session),
        ex=14400
    )

def get_session_list(email=None, token=None):
    if email is not None:
        pattern = "*{}".format(email)
    elif token is not None:
        pattern = "*{}*".format(token)
    else:
        return []

    return app.redis.keys(pattern)

def get_session(email=None, token=None):
    if email is not None:
        pattern = "*{}".format(email)
    elif token is not None:
        pattern = "*{}*".format(token)
    else:
        return None

    keys = app.redis.keys(pattern)
    key = keys[0].decode('ASCII')
    session = app.redis.get(key).decode('ASCII')

    if session is not None:
        return json.loads(session)

    return None

def clear_session(email=None, token=None):
    if email is not None:
        prev_session = get_session_list(email=email)
    elif token is not None:
        prev_session = get_session_list(token=token)

    if len(prev_session) > 0:
        prev_session = list(key.decode('ASCII') for key in prev_session)
        app.redis.delete(*prev_session)

def get_permissions(user):
    if user is not None:
        user_permissions = get_access_permissions(user)

        if user_permissions is not None:
            return user_permissions
        else:
            return []
    return []

def get_user_level(user):
    if user is not None:
        query = {
            "match_phrase": {
                "id_user": str(user)
            }
        }
        queryset = app.elastic.search(
            index="permissions_by_users",
            doc_type="_doc",
            body={
                "query": query
            }
        )
        permissions = list(
            item["_source"] for item in queryset["hits"]["hits"]
        )
        if len(permissions) != 0:
            return permissions[0]['level']
        else:
            return ""
    return ""

def get_jabatan_permissions(jabatan):
    if jabatan is not None:
        query = {
            "match_phrase": {
                "id_jabatan": str(jabatan)
            }
        }
        queryset = app.elastic.search(
            index="jabatan_permission",
            doc_type="_doc",
            body={
                "query": query
            }
        )
        permissions = list(
            item["_source"] for item in queryset["hits"]["hits"]
        )

        if permissions is not None:
            return permissions
        else:
            return []
    return []

def get_access_permissions(user):
    if user is not None:
        query = {
            "match_phrase": {
                "id_user": str(user)
            }
        }
        queryset = app.elastic.search(
            index="access_permission",
            doc_type="_doc",
            body={
                "from": 0,
                "size": 1000,
                "query": query
            }
        )
        permissions = list(
            item["_source"] for item in queryset["hits"]["hits"]
        )

        if permissions is not None:
            return permissions
        else:
            return []
    return []

def get_merged_permissions(user, groups):
    banding = []
    if user is not None and groups is not None:
        from functools import reduce
        permissions = user + groups

        if permissions != banding:
            merged = reduce(
                lambda x, y: compare_permissions(x, y),
                permissions
            )
            return merged
        else:
            return []
    return []

def get_menu(user):
    if user is not None:
        query = {
            "match_all": {}
        }
        queryset = app.elastic.search(
            index="menu",
            doc_type="_doc",
            body={
                "query": query
            })

        menu = list(
            item["_source"] for item in queryset["hits"]["hits"]
        )

        return menu
    return []

def compare_permissions(a, b):
    if type(a) == dict:
        return [
            {
                "kode": a["kode"],
                "nama": a["nama"],
                "deskripsi": a["deskripsi"],
                "permissions": a["permissions"]
            },
            {
                "kode": b["kode"],
                "nama": b["nama"],
                "deskripsi": b["deskripsi"],
                "permissions": b["permissions"]
            },
        ]
    else:
        results = []
        for index, i in enumerate(a):
            if i["kode"] == b["kode"]:
                results.append({
                    "kode": b["kode"],
                    "nama": b["nama"],
                    "deskripsi": b["deskripsi"],
                    "permissions": {
                        "creates":
                            i["permissions"]["creates"] or
                            b["permissions"]["creates"],
                        "reads":
                            i["permissions"]["reads"] or
                            b["permissions"]["reads"],
                        "updates":
                            i["permissions"]["updates"] or
                            b["permissions"]["updates"],
                        "deletes":
                            i["permissions"]["deletes"] or
                            b["permissions"]["deletes"],
                        "imports":
                            i["permissions"]["imports"] or
                            b["permissions"]["imports"],
                        "exports":
                            i["permissions"]["exports"] or
                            b["permissions"]["exports"],
                        "prints":
                            i["permissions"]["deletes"] or
                            b["permissions"]["deletes"]
                    }
                })
            else:
                results.append(i)

        return results

def authenticate(f):
    # Check if user is logged in or return 401
    @wraps(f)
    def func_wrapper(*args, **kwargs):
        # Get real args so it doesnt mix with reqparser
        _realargs = args
        _realkwargs = kwargs
        # Get request headers
        authorization = request.headers.get("Authorization")
        # Throw error if authorization header is not found
        if authorization is None:
            raise Unauthorized()
        # Get token from header
        headers = authorization.split()
        bearer = headers[0]
        token = headers[1]
        # Detect invalid scheme
        if bearer != "Bearer":
            raise InvalidTokenType()
        # Detect invalid token
        session_list = get_session_list(token=token)
        if len(session_list) < 1:
            raise TokenNotFound()
        # Get current session
        session = get_session(token=token)
        if session is None:
            raise TokenNotFound()
        if session['expired'] != 0:
            expired = datetime.strptime(session['expired'], "%Y-%m-%d %H:%M:%S")
            if datetime.now() >= expired:
                raise ExpiredSession()
        _realkwargs.update(session=session)
        try:
            return f(*_realargs, **_realkwargs)
        except TypeError:
            return f(*_realargs)
        except Exception as e:
            print(e)

    return func_wrapper

def auth_permission(fn):
    @wraps(fn)
    def func_wrapper(*args, **kwargs):
        # Get real args so it doesnt mix with reqparser
        _realargs = args
        _realkwargs = kwargs

        authorization = request.headers.get("Authorization")
        headers = authorization.split()
        token = headers[1]

        path = func_wrapper.__module__
        this_module = path.split(".")[0]

        perm = get_session(token=token)

        if perm is not None:
            list_permission = perm['permissions']
            for i in list_permission:
                if i['kode'] == this_module:
                    list_action = i['permissions']
        else:
            print('permission not assigned!')

        # assign permission to methods
        this_action = ""
        this_func = str(func_wrapper.__name__)

        options = {
            'get': 'reads',
            'post': 'creates',
            'put': 'updates',
            'delete': 'deletes'
        }

        this_action = options.get(this_func, 'method not be detected')

        if list_action[this_action] is False:
            raise DontHavePermission()
        try:
            return fn(*_realargs, **_realkwargs)
        except:
            return fn(*_realargs)

        # return fn(*_realargs, **_realkwargs)
    return func_wrapper

def generate_otp(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def generate_key(size=32, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))