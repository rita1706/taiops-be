import tweepy

def init_twitter_con(app):
    # Setup twitter connection used by realtime
    app.logger.info("Initializing Twitter API Connection...")
    config = app.config['TWITTER_CONNECTION']

    if 'twitter_api' not in app.__dict__.keys():
        auth = tweepy.OAuthHandler(config['CONSUMER_KEY'], config['CONSUMER_SECRET'])
        auth.set_access_token(config['ACCESS_TOKEN'], config['ACCESS_TOKEN_SECRET'])
        api = tweepy.API(auth)

        setattr(app, "twitter_api", api)
