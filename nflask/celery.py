"""Celery Background prosses."""

from celery import Celery


def init_celery(app):
    """Celery."""
    app.logger.info("Initializing Celery connection...")
    config = app.config["REDIS"]
    url_ = config['HOST']+ ':'+ str(config['PORT'])
    celery = Celery(
        app.import_name,
        backend='redis://'+url_,
        broker='redis://'+url_
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery
