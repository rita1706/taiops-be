from nflask.offline_data_handler import save_to_elassandra, get_data_from_elassandra
import concurrent.futures
from nflask.mixins.resources import ElasticListResourceMixin as elastic

def get_alias_data(data):
	# elastic_index = "entity_aliasing"
	# elastic_doc_type = "_doc"
	# elastic_doc_id = "default"
	# data = get_data_from_elassandra(f'{elastic_index}/{elastic_doc_type}/{elastic_doc_id}', {}).json()
	elastic_doc_id = "default"
	data = data['hits']['hits'][0] if data['hits']['hits'] else ['error']
	if 'error' not in data:
		source = data.get('_source',{})
		id_row = data['_id']
		username = source.get('username','default')
		alias = source.get('alias',{})
		alias = [alias[x] for x in alias] # {1:[],2:[]} into [[],[]] !!! masih ada string diantara list
		fixed_alias = []
		for ali in alias:
			if type(ali) == type(""):
				ali = [ali]
			fixed_alias.append(ali)
		doc = {
			"username":username,
			"alias":fixed_alias
		}
		DATA_ALIAS = doc
	else:
		DATA_ALIAS = {
			"username":elastic_doc_id,
			"alias":[]
		}
	return DATA_ALIAS

def find_in_list(top, top_value, list_alias): # jokowi, 10, [['','',''],['','']]
	join_alias=[]
	for alias in list_alias: #[] in [[],[]]
		if top.upper() in [x.upper() for x in alias]:
			join_alias.extend(alias)
	
	join_alias = list(set(join_alias))
#	 return (top,top_value,join_alias)
	return {
		'entity':join_alias[0] if join_alias[0:] else top,
		'value':top_value,
		'alias':join_alias
	}

#cari alias
def find_alias(dict_top,data):
	print(1)
	list_alias = get_alias_data(data)['alias']
	print(2,list_alias)
	# print(list_alias)
		
 # gabungkan yang dalam satu alias
 # gunakan value terbanyak yg terdapat pada alias group
	aliased={} # {'a_A': {'entity': 'a', 'value': 3, 'alias': ['a', 'A']}, '': {'entity': 'jokowidodo', 'value': 15, 'alias': []}}
	with concurrent.futures.ThreadPoolExecutor() as executor:
		task=[]
		dict_top = dict_top['dict_top']
		for key in dict_top:
			# print(key)
			task.append(executor.submit(find_in_list, key, dict_top[key], list_alias))
		for t in task:
			a = t.result()
			key = list(set([a['entity']] + a['alias']))
			key.sort()
			key = '_'.join(key)
			if key not in aliased:
				aliased[key] = a
			else:
				if a['value'] > aliased[key]['value']:
					aliased[key] = a
	return {'aliased': [aliased[key] for key in aliased]}