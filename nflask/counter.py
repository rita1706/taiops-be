def count_from_model(Model):
    try:
        return Model.objects.filter(is_deleted=False).count()
    # flake8: noqa
    except:
        print("Count records failed")
        return 0

def save_records(module):
    pass

def count_records(app, module, Model):
    count = count_from_model(Model)
